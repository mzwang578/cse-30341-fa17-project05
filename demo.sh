#!/usr/bin/env bash

for script in ./demo/*; do
    echo -e "\n--- Testing ${script}..."
    ./$script
done
