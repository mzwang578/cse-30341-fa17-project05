CSE.30341.FA17: Project 05
==========================

This is the documentation for [Project 05] of [CSE.30341.FA17].

Members
-------

1. Michael Wang (mwang6@nd.edu)
2. Anthony Luc (aluc@nd.edu)
3. Donald Luc (dluc@nd.edu)

Design
------

> 1. In the `HTCache` constructor, you will need to determine the following
>    values: `Addresses`, `Pages`, `VPNShift`, `VPNMask`, and `OffsetMask`.
>    How would you determine these values and what would they be used for?

You can determine the number of Addresses is 2^addrlen. You will use this to determine the total number of entries. You will get Pages by taking the number of addresses and dividing it by pagesize. 
The VPNShift is determined by log base 2 of the page size, this is used to differentiate between the VPN and OFFSET. 
Theoretically, we would use a bit mask operation and shift operation using the << and & symbols to parse the offset and vpn from the address. These will methods will serve as our VPNMask and OffsetMask. 

> 2. In the `HTCache::get` method, you will need to determine the **VPN** and
>    **offset** based on the specified **key**.
> 
>       - How would you determine these values?
>       - How would you use these values to retrieve the specified value?
>       - What data would you need to update?
>       - What data would you need to synchronize?

You can determine the VPN and offset using the VPNMask and OffsetMask and also the VPNSHFIT on the key to obtain the necessary amounts of bits which correlate to the HTPage and Entry that you want to get from. The VPN correlates to what HTPage you would go into and the Offset will be which Entry within the HTPage you need. If the value does not exist in the entry/HTPage, then you will need to update the entry by calling the handler to generate or retrieve the missing value which will be updated within the entry. You will need to synchronize whenever we retrieve or update an entry to make sure that it is asyncrhonous and it is thread-safe.

> 3. In the `HTCache::put` method, you will need to determine the **VPN** and
>    **offset** based on the specified **key**.
> 
>       - How would you determine these values?
>       - How would you use these values to store the specified value?
>       - What data would you need to update?
>       - What data would you need to synchronize?

We will use the same methods to get the VPN and offset as above in question 2 based on the specified key. For the put method, the data you would need to update is the existing value associated with the key for the entry, these are obtained by the VPN and offset where the VPN is the HTPage and the offset is the entry. However, you will need to use linear probing if the entry is already taken up. We will continue to do linear probing and if no empty entry is found, then we know that the HTCache is full for the existing entry, and then you will need to perform an eviction of a certain entry based on the policy to replace it with the new value given from the key. Again, you will need to implement thread-safe retreive and update for the entry to ensure it is thread-safe.

> 3. In the `HTPage` class, you will need to implement FIFO, Random, LRU, and
>    Clock eviction policies.  For each of these policies:
> 
>       - What bookkeeping do you need to implement the policy?
>       - How would you use this bookkeeping to implement the policy?

For FIFO, we will keep track of the time when the entry is first created. When an entry needs to be evicted, we will find the earliest time and evict that one. 
For Random, we don't need to keep track of anything except for a randomly generated number corresponding to which entry needs to be evicted. This number doesn't depend on time, order, etc. so when an entry needs to be evicted, we will just use a random number.
For LRU, we will keep track of a second time which can be updated whenever an entry is accessed (as opposed to the FIFO timestamp which won't be updated). When an entry needs to be evicted, we will find the earliest time and evict that one. 
For Clock, we will keep track of a dirty bit that can either be 1 or 0. When we add a entry to our Page table, we will set the bit value to 1. When we are looking for an entry to evict, we will traverse through our entries as long as the dirty bit is 1, set the dirty bit to 0 as we access the entry, and if we find an entry with a value of 0, evict that entry. 


> 5. In the `HTPage::get`, you must use linear probing to locate an appropriate
>    `HTPageEntry`.
> 
>       - How would you determine if you found the correct `HTPageEntry`?

If the value of the entry is filled and it isn't the value you are looking for, look through the next entries and go until you find the value or an open entry.

Response.

> 6. In the `HTPage::put`, you must use linear probing to locate an appropriate
>    `HTPageEntry`.
> 
>       - How would you determine if you need to update a value rather than add a new one?
>       - How would you determine if you need to perform an eviction?
>       - How would you perform an eviction?

You would need to add a new value if no HTPageEntry is found. You will need to update the HTPageEntry that the eviction policy returns when the HTPageEntry is full and you will need to linear probe for both finding the entry to update and also determining whether the HTPageEntry is full. You would determine if you need to perform an eviction by linear probing to see if all the entries are full. You would perform an eviction by replacing an existing entry with the new entry from the put request.

Demonstration
-------------

> See [Google Drive] below.

Errata
------

> We were able to pass all the demo scripts in terms of what is to be expected. Sometimes our hits and misses for fibonacci, selection, and insertion are off slightly so our scripts would say they failed, but the range which the hits and misses are in compared to what the test script provides are to be expected. 
Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 05]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project05.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://docs.google.com/presentation/d/1rTr67k803FFJVcn6pDIyiNHX9xgpNUSWea-yFEwXsRA/edit#slide=id.g2b551585af_0_130
