#!/usr/bin/env bash

for binary in ./bin/*; do
    echo -e "\n--- Testing ${binary}..."
    ./$binary 10 64 0 1
done
