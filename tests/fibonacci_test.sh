#!/usr/bin/env bash
# 
# Operating Systems
# Project05: Memcache
# fibonacci_test.sh

FIB_BIN="bin/fibonacci"
TESTS_TXT_DIR="tests/txtFiles"

# Clean txt files.
rm -rf $TESTS_TXT_DIR

mkdir $TESTS_TXT_DIR

if make $FIB_BIN; then
    # How does the size of the cache (ie. AddressLength and PageSize) impact the performance?
    # Which eviction policy is provides the best performance?
    # How does having multiple threads impact the cache behavior?

    for thread in 1 2 4 8; do
        for policy in 0 1 2 3; do
            TXT_FILE=$TESTS_TXT_DIR/policy_${thread}_${policy}.txt
            echo -e "********* Thread:" $thread " Policy:" $policy "*********************\n" | tee -a $TXT_FILE
            for page_size in 4 8 16 32 64 128; do
                echo "********* Page Size:" $page_size " ************" | tee -a $TXT_FILE
                for addr_len in 8 10 16 24; do
                    echo "###### Address Length:" $addr_len " ######" | tee -a $TXT_FILE
                    ./$FIB_BIN $addr_len $page_size $policy $thread | grep 'Hits' | echo "Hits:" `awk '{print $3}'` | tee -a $TXT_FILE
                    ./$FIB_BIN $addr_len $page_size $policy $thread | grep 'Misses' | echo "Misses:" `awk '{print $3}'` | tee -a $TXT_FILE
                    (time ./$FIB_BIN $addr_len $page_size $policy $thread) 2>&1 | grep 'real' | echo "Real:" `awk '{print $2}'` | tee -a $TXT_FILE
                    echo "" | tee -a $TXT_FILE
                done
            done
        done
    done
fi
