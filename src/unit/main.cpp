// SYSTEM INCLUDES

#include <gtest/gtest.h>

#include <htcache/htcache.h>

#include <assert.h>

#define N (1<<2)

uint64_t Data[N] = {0};

/* Begin Unit Tests -------------------------------*/
/* HTCache Tests ----------------------------------*/

TEST(HTCacheTest, GetFunction) {
    int addrlen_array[5] = {4, 8, 10, 16, 20};
    int pagesize_array[5] = {4, 8, 16, 32, 64};

    uint64_t retVal = 0;

    // for (int p = 0; p < 4; ++p) {  // Policy
    //     EvictionPolicy policy = static_cast<EvictionPolicy>(p);
    //     for (int a = 0; a < 5; ++a) {  // Address Length
    //         for (int ps = 0; ps < 5; ++ps) {  // Pagesize
    //             HTCache<uint64_t, uint64_t> cache(addrlen_array[a], pagesize_array[ps], policy, [](const uint64_t &key) { return Data[key%N]; });

    //             retVal = cache.get(0);
    //             EXPECT_EQ ((uint64_t)0, retVal);

    //             retVal = cache.get(1);
    //             EXPECT_EQ ((uint64_t)1, retVal);

    //             retVal = cache.get(99);
    //             EXPECT_EQ ((uint64_t)3, retVal);

    //             retVal = cache.get(-1);
    //             EXPECT_EQ ((uint64_t)3, retVal);
    //         }
    //     }
    // }
}

TEST(HTCacheTest, PutFunction) {
    long int addrlen = 8;
    long int pagesize = 64;
    EvictionPolicy policy = static_cast<EvictionPolicy>(0);
 
    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, [](const uint64_t &key) { return Data[key%N]; });

    uint64_t retVal = 0;

    cache.put(0, 1);
    retVal = cache.get(0);
    EXPECT_EQ ((uint64_t)1, retVal);

    cache.put(2, 9);
    retVal = cache.get(2);
    EXPECT_EQ ((uint64_t)9, retVal);

    cache.put(99, 99);
    retVal = cache.get(99);
    EXPECT_EQ ((uint64_t)99, retVal);

    cache.put(-1, -1);
    retVal = cache.get(-1);
    EXPECT_EQ ((uint64_t)-1, retVal);
}

TEST(CacheEvictionPolicyTest, FIFO) {
    long int addrlen = 4;
    long int pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(0); // FIFO

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, [](const uint64_t &key) { return Data[key%N]; });

    cache.put(0, 1);
    cache.put(1, 2);
    cache.put(2, 3);
    cache.put(3, 4);

    uint64_t retVal = 0;

    retVal = cache.get(0);
    EXPECT_EQ ((uint64_t)1, retVal);
    retVal = cache.get(1);
    EXPECT_EQ ((uint64_t)2, retVal);
    retVal = cache.get(2);
    EXPECT_EQ ((uint64_t)3, retVal);
    retVal = cache.get(3);
    EXPECT_EQ ((uint64_t)4, retVal);

    cache.put(17, 2);
    retVal = cache.get(17);
    EXPECT_EQ ((uint64_t)2, retVal); 
}

TEST(CacheEvictionPolicyTest, LRU) {
    long int addrlen = 4;
    long int pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(1); // LRU

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, [](const uint64_t &key) { return Data[key%N]; });

    cache.put(0, 1);
    cache.put(1, 2);
    cache.put(2, 3);
    cache.put(3, 4);

    uint64_t retVal = 0;

    retVal = cache.get(0);
    EXPECT_EQ ((uint64_t)1, retVal);
    retVal = cache.get(1);
    EXPECT_EQ ((uint64_t)2, retVal);
    retVal = cache.get(2);
    EXPECT_EQ ((uint64_t)3, retVal);
    retVal = cache.get(3);
    EXPECT_EQ ((uint64_t)4, retVal);

    cache.put(17, 2);
    retVal = cache.get(17);
    EXPECT_EQ ((uint64_t)2, retVal); 
}

TEST(CacheEvictionPolicyTest, RANDOM) {
    long int addrlen = 4;
    long int pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(2); // RANDOM

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, [](const uint64_t &key) { return Data[key%N]; });

    cache.put(0, 1);
    cache.put(1, 2);
    cache.put(2, 3);
    cache.put(3, 4);

    uint64_t retVal = 0;

    retVal = cache.get(0);
    EXPECT_EQ ((uint64_t)1, retVal);
    retVal = cache.get(1);
    EXPECT_EQ ((uint64_t)2, retVal);
    retVal = cache.get(2);
    EXPECT_EQ ((uint64_t)3, retVal);
    retVal = cache.get(3);
    EXPECT_EQ ((uint64_t)4, retVal);

    cache.put(17, 2);
    retVal = cache.get(17);
    EXPECT_EQ ((uint64_t)2, retVal); 
}

TEST(CacheEvictionPolicyTest, CLOCK) {
    long int addrlen = 4;
    long int pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(3); // CLOCK

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, [](const uint64_t &key) { return Data[key%N]; });

    cache.put(0, 1);
    cache.put(1, 2);
    cache.put(2, 3);
    cache.put(3, 4);

    uint64_t retVal = 0;

    retVal = cache.get(0);
    EXPECT_EQ ((uint64_t)1, retVal);
    retVal = cache.get(1);
    EXPECT_EQ ((uint64_t)2, retVal);
    retVal = cache.get(2);
    EXPECT_EQ ((uint64_t)3, retVal);
    retVal = cache.get(3);
    EXPECT_EQ ((uint64_t)4, retVal);

    cache.put(17, 2);
    retVal = cache.get(17);
    EXPECT_EQ ((uint64_t)2, retVal); 
}


/* HTPage Tests -----------------------------------*/
TEST(HTPageTest, GetPutFunction) {
    size_t pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(0);

    HTPage<uint64_t, uint64_t> page(pagesize, policy);

    page.put((uint64_t)1, (uint64_t)0, (uint64_t)0);
    page.put((uint64_t)2, (uint64_t)1, (uint64_t)1);
    page.put((uint64_t)3, (uint64_t)2, (uint64_t)2);
    page.put((uint64_t)4, (uint64_t)3, (uint64_t)3);

    uint64_t retVal = (uint64_t)0;
    retVal = page.get((uint64_t)1, (uint64_t)0);
    EXPECT_EQ ((uint64_t)0, retVal);

    retVal = page.get((uint64_t)2, (uint64_t)1);
    EXPECT_EQ ((uint64_t)1, retVal);

    retVal = page.get((uint64_t)3, (uint64_t)2);
    EXPECT_EQ ((uint64_t)2, retVal);

    retVal = page.get((uint64_t)4, (uint64_t)3);
    EXPECT_EQ ((uint64_t)3, retVal);
}

TEST(HTPageTest, FIFO) {
    size_t pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(0);

    HTPage<uint64_t, uint64_t> page(pagesize, policy);

    page.put((uint64_t)1, (uint64_t)0, (uint64_t)0);
    page.put((uint64_t)2, (uint64_t)1, (uint64_t)1);
    page.put((uint64_t)3, (uint64_t)2, (uint64_t)2);
    page.put((uint64_t)4, (uint64_t)3, (uint64_t)3);

    // Do an eviction put. Try to put key 5 into offset 3.
    page.put((uint64_t)5, (uint64_t)3, (uint64_t)3);


    // We expect Offset 0 (the first in) should now have Key of 5.
    uint64_t retVal = (uint64_t)0;
    retVal = page.get((uint64_t)5, (uint64_t)0);
    EXPECT_EQ ((uint64_t)3, retVal);

    // We expect remaining offsets should have the same value.
    retVal = page.get((uint64_t)2, (uint64_t)1);
    EXPECT_EQ ((uint64_t)1, retVal);

    retVal = page.get((uint64_t)3, (uint64_t)2);
    EXPECT_EQ ((uint64_t)2, retVal);

    retVal = page.get((uint64_t)4, (uint64_t)3);
    EXPECT_EQ ((uint64_t)3, retVal);
}

TEST(HTPageTest, LRU) {
    size_t pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(1);

    HTPage<uint64_t, uint64_t> page(pagesize, policy);

    page.put((uint64_t)1, (uint64_t)0, (uint64_t)0);
    page.put((uint64_t)2, (uint64_t)1, (uint64_t)1);
    page.put((uint64_t)3, (uint64_t)2, (uint64_t)2);
    page.put((uint64_t)4, (uint64_t)3, (uint64_t)3);

    uint64_t retVal = (uint64_t)0;

    // Make offset 2 the least recently used.
    retVal = page.get((uint64_t)3, (uint64_t)0);
    retVal = page.get((uint64_t)3, (uint64_t)1);
    retVal = page.get((uint64_t)3, (uint64_t)3);


    // Do an eviction put. Try to put key 5 into offset 0.
    page.put((uint64_t)5, (uint64_t)3, (uint64_t)0);


    // We expect Offset 2 (the LRU) should now have Key of 5.
    retVal = page.get((uint64_t)5, (uint64_t)2);
    EXPECT_EQ ((uint64_t)3, retVal);

    // We expect remaining offsets should have the same value.
    retVal = page.get((uint64_t)1, (uint64_t)0);
    EXPECT_EQ ((uint64_t)0, retVal);

    retVal = page.get((uint64_t)2, (uint64_t)1);
    EXPECT_EQ ((uint64_t)1, retVal);

    retVal = page.get((uint64_t)4, (uint64_t)3);
    EXPECT_EQ ((uint64_t)3, retVal);
}

TEST(HTPageTest, RANDOM) {
    size_t pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(2);

    HTPage<uint64_t, uint64_t> page(pagesize, policy);    

    // How do you test random when you don't know which is evicted?
    EXPECT_EQ (1, 1);
}

TEST(HTPageTest, CLOCK) {
    size_t pagesize = 4;
    EvictionPolicy policy = static_cast<EvictionPolicy>(3);

    HTPage<uint64_t, uint64_t> page(pagesize, policy);


    page.put((uint64_t)1, (uint64_t)0, (uint64_t)0);
    page.put((uint64_t)2, (uint64_t)1, (uint64_t)1);
    page.put((uint64_t)3, (uint64_t)2, (uint64_t)2);
    page.put((uint64_t)4, (uint64_t)3, (uint64_t)3);

    uint64_t retVal = (uint64_t)0;

    // Make offset 2 the least recently used.
    retVal = page.get((uint64_t)3, (uint64_t)1);
    retVal = page.get((uint64_t)3, (uint64_t)2);

    // Do an eviction put. Try to put key 5 into offset 0.
    page.put((uint64_t)5, (uint64_t)3, (uint64_t)0);

    // We expect Offset 1 to now have Key of 5.
    retVal = page.get((uint64_t)5, (uint64_t)1);
    EXPECT_EQ ((uint64_t)3, retVal);

    // We expect remaining offsets should have the same value.
    retVal = page.get((uint64_t)2, (uint64_t)1);
    EXPECT_EQ ((uint64_t)1, retVal);

    retVal = page.get((uint64_t)3, (uint64_t)2);
    EXPECT_EQ ((uint64_t)2, retVal);

    retVal = page.get((uint64_t)4, (uint64_t)3);
    EXPECT_EQ ((uint64_t)3, retVal);

}

int main(int argc, char* argv[]) {
    for (size_t i = 0; i < N; i++) {
        Data[i] = i;
    }

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
