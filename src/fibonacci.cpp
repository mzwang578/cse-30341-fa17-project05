// fibonacci.cpp

#include <htcache/htcache.h>

#include <cstdio>
#include <iomanip>
#include <thread>

#define N 100

/* Global */
HTCache<uint64_t, uint64_t> *global_cache;

uint64_t fibonacci_handler(const uint64_t &key) {
    if (key == 0) {
        return 0;
    }
    if (key == 1) {
        return 1;
    }

    return global_cache->get(key - 1) + global_cache->get(key - 2);
}

void fibonacci_thread(HTCache<uint64_t, uint64_t> *cache, size_t thread_num) {
    for (uint64_t i = 0; i < N; i++) {
        std::cout << " " << thread_num << ". Fibonacci(" << std::setw(2) << i << ") = " << cache->get(i) << std::endl;
    }
}
    
int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    size_t addrlen 		  = strtol(argv[1], NULL, 10);
    size_t pagesize 	  = strtol(argv[2], NULL, 10);
    EvictionPolicy policy = static_cast<EvictionPolicy>(strtol(argv[3], NULL, 10));
    size_t nthreads       = strtol(argv[4], NULL, 10);

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, fibonacci_handler);
    global_cache = &cache;    

    std::thread t[nthreads];
    for (size_t i = 0; i < nthreads; i++) {
    	t[i] = std::thread(fibonacci_thread, global_cache, i);
    }

    for (size_t i = 0; i < nthreads; i++) {
    	t[i].join();
    }

    cache.stats(stdout);
    fflush(stdout);
    return EXIT_SUCCESS;
}
