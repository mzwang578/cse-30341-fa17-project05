// htpage.h: Hash Tree Page

#pragma once

#include <time.h>

#include <cstdlib>
#include <mutex>
#include <queue>
#include <random>
#include <stdexcept>
#include <vector>
#include <iostream>


enum EvictionPolicy {
    EVICT_FIFO,
    EVICT_RANDOM,
    EVICT_LRU,
    EVICT_CLOCK,
};

template <typename KeyType, typename ValueType>
struct HTPageEntry {
    KeyType	Key;
    ValueType	Value;
    
    // Add bookkeeping for eviction
    size_t created;
    size_t last_used;
    int    dirty_bit;
    bool   empty;
};

template <typename KeyType, typename ValueType>
class HTPage {
    typedef std::vector<HTPageEntry<KeyType, ValueType>> EntriesType;

private:
    EntriesType		Entries;       // Individual entries
    EvictionPolicy	Policy;	       // Eviction policy
    mutable std::mutex	Lock;	   // Lock

    size_t created_counter   = 0;  // Counter for created.
    size_t last_used_counter = 0;  // Countetr for last_used.

    // Add bookkeeping for eviction
    size_t evict_fifo(size_t offset) {
        size_t time_created = Entries[offset].created;
    	// Implement FIFO eviction policy
        for (size_t i = 0; i < Entries.size(); i++) {
            size_t index = (offset + i) % Entries.size();
            if (Entries[index].created < time_created) {
                time_created = Entries[index].created;
                offset = index;
            }
        }
    	return offset;
    }

    size_t evict_random(size_t offset) {
    	// Implement random eviction policy
        std::default_random_engine engine;
        std::uniform_int_distribution<> random(0, Entries.size());

        size_t random_int = (rand() % Entries.size());
        while (random_int == offset) {
            random_int = (random(engine) % Entries.size());
        }
        offset = random_int;
    	return offset;
    }

    size_t evict_lru(size_t offset) {
        size_t time_last_used = Entries[offset].last_used;
        // Implement LRU eviction policy
        for (size_t i = 0; i < Entries.size(); i++) {
            size_t index = (offset + i) % Entries.size();
            if (Entries[index].last_used < time_last_used) {
                time_last_used = Entries[index].last_used;
                offset = index;
            }
        }
    	return offset;
    }

    size_t evict_clock(size_t offset) {
    	// Implement clock eviction policy
        for (size_t i = offset; i < Entries.size(); i++) {
            size_t index = (offset + i) % Entries.size();
            if (Entries[index].dirty_bit == 0) {
                offset = index;
                break;
            }
            else {
                Entries[index].dirty_bit = 0;
            }
        }
    	return offset;
    }

public:
    HTPage(size_t n, EvictionPolicy p) {
    	// Initialize Entries
        for (size_t i = 0; i < n; ++i) {
            HTPageEntry<KeyType, ValueType> entry;
            entry.empty = true;
            entry.dirty_bit = 1;
            Entries.push_back(entry);
        }
        Policy = p;
    }

    HTPage(const HTPage<KeyType, ValueType>& other) {
        std::lock_guard<std::mutex> other_guard(other.Lock);
        std::lock_guard<std::mutex> guard(Lock);
        Entries = other.Entries;
        Policy = other.Policy;
    }

    ValueType get(const KeyType &key, size_t offset) {
        std::lock_guard<std::mutex> guard(Lock);
        
        // Use linear probing to locate key
        bool flag = false;
        ValueType value;

        for (size_t i = 0; i < Entries.size(); ++i) {
            size_t index = (offset + i) % Entries.size();
            if (Entries[index].empty) {
                break;
            }
            else if (!Entries[index].empty && Entries[index].Key != key) {
                continue; //linear probe
            }
            else if (!Entries[index].empty && Entries[index].Key == key) {
                value = Entries[index].Value;
                Entries[index].last_used = last_used_counter++;

                flag = true;
                break;
            }
        }

        if (!flag)
            throw std::out_of_range("Not in cache");

        return value;
    }

    void put(const KeyType &key, const ValueType &value, size_t offset) {
        std::lock_guard<std::mutex> guard(Lock);

        // Use linear probing to locate key
        bool flag = false;
        for (size_t i = 0; i < Entries.size(); ++i) {
            size_t index = (offset + i) % Entries.size();
            if (Entries[index].empty) { // Empty Entry
                Entries[index].created = created_counter++;
                Entries[index].last_used = last_used_counter++;
                Entries[index].Key = key;
                offset = index;
                flag = true;
                Entries[index].empty = false;
                break;
            }
            else { // Entry not empty, check the key.
                if (Entries[index].Key != key) { // Not the key
                    continue; // Linear Probe
                }
                else { // Found the key
                    Entries[index].last_used = last_used_counter++;
                    offset = index;
                    flag = true;
                    Entries[index].empty = false;
                    break;
                }
            }
        }

    	// Evict an entry if HTPage is full
        if (!flag) {
            switch (Policy) {
                case EVICT_FIFO:
                    offset = evict_fifo(offset);
                    break;
                case EVICT_RANDOM:
                    offset = evict_random(offset);
                    break;
                case EVICT_LRU:
                    offset = evict_lru(offset);
                    break;
                case EVICT_CLOCK:
                    offset = evict_clock(offset);
                    break;
                default:
                    break;
            }
            Entries[offset].Key = key;
            Entries[offset].created = created_counter++;
            Entries[offset].last_used = last_used_counter++;
        }

	    // Update entry
        Entries[offset].dirty_bit = 1;
        Entries[offset].Value = value;

    }
};
