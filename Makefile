CXX=		g++
CXXFLAGS=	-g -gdwarf-2 -std=gnu++11 -Wall -Iinclude
LDFLAGS=	-lpthread
UD=			g++
UDFLAGS=	-Llib
HEADERS=	$(wildcard include/htcache/*.h)
APPS=		$(patsubst src/%,bin/%,$(patsubst %.cpp,%,$(wildcard src/*.cpp)))
TESTS=		$(patsubst tests/%,bin/%,$(patsubst %.cpp,%,$(wildcard tests/*.cpp)))

# These are macros for linking the already compiled gtest libraries.
GTEST_INC_DIR	= contrib/gtest/googletest/include
GTEST_LNK_PATH	= build/googlemock/gtest
GTEST_LNK_FLAGS = -lgtest -lgtest_main
GTEST			= build/googlemock/gtest

# Unit test macros.
UNIT_FLAGS		= -I$(GTEST_INC_DIR)
UNIT_LNK_FLAGS	= -L$(GTEST_LNK_PATH) $(GTEST_LNK_FLAGS) $(LNKFLAGS) -pthread
UNIT_SOURCE		= $(wildcard src/unit/*.cpp)
UNIT_HEADERS	= $(wildcard src/unit/*.h)
UNIT_OBJECTS	= $(UNIT_SOURCE:.cpp=.o)
UNIT			= bin/client_unit


all:	$(APPS) $(TESTS) $(GTEST) $(UNIT)

bin/%:		src/%.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

bin/test_%:	tests/test_%.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

$(UNIT):	$(UNIT_OBJECTS) $(GTEST)
	$(UD) -o $@ $(UNIT_OBJECTS) $(UDFLAGS) $(UNIT_LNK_FLAGS)

$(GTEST):	contrib/gtest
	(mkdir build; cd build; cmake ../contrib/gtest; make)

src/unit%.o:	src/unit%.cpp
	$(CXX) $(CXXFLAGS) $(UNIT_FLAGS) -o $@ -c $<

tests:	$(TESTS)

clean:
	rm -rf build $(APPS) $(TESTS) $(GTEST) $(UNIT) tests/*.o

PHONY: all clean
